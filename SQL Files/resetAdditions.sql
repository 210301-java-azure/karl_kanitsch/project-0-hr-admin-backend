
alter sequence employees_employee_id_seq restart with 51;
alter sequence user_accounts_userid_seq restart with 51;
delete from user_account where userid > 50;
delete from employee where employee_id > 50;
insert into employee (first_name, last_name, pay_rate, federal_id, hire_date) values ('Delete', 'Me', 22.50, '987-65-4321', current_date);
update user_account set employee_id = 14 where userid = 2;


