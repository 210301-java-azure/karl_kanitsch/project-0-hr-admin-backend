package dev.kkanit.Apps;

import dev.kkanit.models.Localizer;
import io.javalin.Javalin;
import io.javalin.http.Context;
import dev.kkanit.models.HelloPage;
import java.util.*;
import java.util.Map;

public class App {

    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7000);
        app.get("/", App::renderHelloPage);
    }

    private static void renderHelloPage(Context ctx) {
        HelloPage page = new HelloPage();
        page.userName = "<script>alert('xss')</script>";
        page.userKarma = 1337;
        Map pageMap = new HashMap();
        pageMap.put("page", page);
        pageMap.put("localizer", new Localizer(Locale.US));
        ctx.render("hello.jte", pageMap);
    }
}
