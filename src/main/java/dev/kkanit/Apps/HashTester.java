package dev.kkanit.Apps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class HashTester {

    private static PasswordEncoder passwordEncoder = encoder();

    public static void main(String[] args) {
        try {
            String testPW = "password1";
            System.out.println(testPW);
            System.out.println(passwordEncoder.encode(testPW));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static PasswordEncoder encoder() {
        System.out.println("PasswordEncoder creation instantiated");
        return new BCryptPasswordEncoder();
    }
}
