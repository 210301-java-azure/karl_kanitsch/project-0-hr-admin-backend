package dev.kkanit;

import dev.kkanit.controllers.AuthController;
import dev.kkanit.controllers.EmployeeController;
import dev.kkanit.controllers.UserController;
import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;

public class Driver {

    static JavalinApp app = new JavalinApp();
    public static void main(String[] args) {
//        userController.hashAllPasswords();
        app.start();
    }
}
