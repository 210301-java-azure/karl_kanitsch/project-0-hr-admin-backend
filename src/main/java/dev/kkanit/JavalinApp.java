package dev.kkanit;

import dev.kkanit.controllers.AuthController;
import dev.kkanit.controllers.EmployeeController;
import dev.kkanit.controllers.UserController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {

    private static final int PORT = 7000;

    AuthController authController = new AuthController();
    UserController userController = new UserController();
    EmployeeController employeeController = new EmployeeController();

    Javalin app = Javalin.create().routes(() -> {
        path("login", () -> {
            get(authController::handleLoginGet);
            post(authController::handleAuthenticateUser);
        });
        path("register", ()->{
            post(userController::handleNewUser);
        });
        path("employee", ()->{
            get(employeeController::handleGetRequest);
            put(employeeController::handlePutEmployeeRequest);
            delete(employeeController::handleDeleteEmployeeRequest);
            path(":id", () -> {
                get(employeeController::handleGetEmployeeRequest);
            });
        });
        path("user", ()->{
           get(userController::handleGetUserRequest);
           post(userController::handlePostUserRequest);
           path("change-password", ()->{
               post(userController::handleChangePassword);
           });
        });
    });

    public void start() {
        this.app.start(PORT);
    }

    public void stop() {
        this.app.stop();
    }
}
