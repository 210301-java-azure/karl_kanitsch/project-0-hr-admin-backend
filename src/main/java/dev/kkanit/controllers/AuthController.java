package dev.kkanit.controllers;

import dev.kkanit.services.PrivilegeService;
import dev.kkanit.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class AuthController {

    private String username = "";
    private String password = "";
    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    private UserService userService = new UserService();
    private PrivilegeService privilegeService = new PrivilegeService();

    public void handleAuthenticateUser(Context ctx) {
        if (ctx.formParamMap()==null || ctx.formParam("username")==null || ctx.formParam("password")==null)
            throw new UnauthorizedResponse("You must enter a valid Username and Password");
        username = ctx.formParam("username");
        password = ctx.formParam("password");
        logger.info("Authenticating user with username: " + username);
        int privilege = userService.authenticate(username, password);
        if (privilege > 0) {
            logger.info("User " + username + " authorized with privilege level " + privilege);
            String authToken = userService.generateAuthToken(username, privilege);
            ctx.json(authToken);
//            ctx.result("Welcome, " + username);
        }
        else {
            logger.warn("Unauthorized login attempt under username: " + username);
            throw new UnauthorizedResponse("Invalid login credentials submitted");
        }
    }

    public void handleLoginGet(Context ctx) {
        String resultString;
        if (ctx.headerMap()!=null && ctx.headerMap().containsKey("auth-token")) {
            String token = ctx.headerMap().get("auth-token");
            String authToken = token.replaceAll("^\"+|\"+$", "");
            Map<String, Object> claimsMap = userService.validateAuthToken(authToken);
            if (claimsMap.containsKey("username"))
                resultString = "Welcome, " + claimsMap.get("username") + "\nYou currently have privilege: " + claimsMap.get("privilege");
            else {
                throw new UnauthorizedResponse("You must login to perform a get request on this resource.");
            }
        }
        else
            throw new UnauthorizedResponse("You must login to perform a get request on this resource.");
        ctx.result(resultString);
    }

    public int getPrivilege(Context ctx) {
        if (ctx.headerMap()!=null && ctx.headerMap().containsKey("auth-token")) {
            String token = ctx.headerMap().get("auth-token");
            String authToken = token.replaceAll("^\"+|\"+$", "");
            Map<String, Object> claimsMap = userService.validateAuthToken(authToken);
            if (claimsMap.containsKey("privilege"))
                return Integer.parseInt(claimsMap.get("privilege").toString());
        }
        return 0;
    }

    public String getUsername(Context ctx) {
        if (ctx.headerMap()!=null && ctx.headerMap().containsKey("auth-token")) {
            String token = ctx.headerMap().get("auth-token");
            String authToken = token.replaceAll("^\"+|\"+$", "");
            Map<String, Object> claimsMap = userService.validateAuthToken(authToken);
            if (claimsMap.containsKey("username"))
                return (String) claimsMap.get("username");
        }
        return "";
    }

    public boolean hasHRAccess(Context ctx) {
        boolean hasAccess = false;
        int privilege = getPrivilege(ctx);
        hasAccess = privilegeService.hasHRAccess(privilege);
        return hasAccess;
    }

    public boolean hasUserAccess(Context ctx) {
        boolean hasAccess = false;
        int privilege = getPrivilege(ctx);
        hasAccess = privilegeService.hasUserAccess(privilege);
        return hasAccess;
    }

    public boolean hasAdminAccess(Context ctx) {
        boolean hasAccess = false;
        int privilege = getPrivilege(ctx);
        hasAccess = privilegeService.hasAdminAccess(privilege);
        return hasAccess;
    }
}
