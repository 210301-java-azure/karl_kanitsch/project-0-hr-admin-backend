package dev.kkanit.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kkanit.services.EmployeeService;
import dev.kkanit.services.PrivilegeService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class EmployeeController {

    private AuthController authController = new AuthController();
    private EmployeeService employeeService = new EmployeeService();
    private Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private ObjectMapper mapper = new ObjectMapper();

    public void handleGetRequest(Context ctx) {
        if (authController.hasHRAccess(ctx)) {
            try {
                ctx.result(mapper.writeValueAsString(employeeService.getAllEmployees()));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        else if (authController.hasUserAccess(ctx)) {
            String user = authController.getUsername(ctx);
            try {
                ctx.result(mapper.writeValueAsString(employeeService.getEmployeeByUsername(user)));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        else
            ctx.result("You must log in first to view employee information");
    }

    public void handleGetEmployeeRequest(@NotNull Context ctx) {
        logger.info("Trying to get employee data for employee id: " + ctx.pathParam("id"));
        int employeeID = 0;
        try {
            employeeID = Integer.parseInt(ctx.pathParam("id"));
        } catch (NumberFormatException e) {
            logger.error("Could not parse " + ctx.pathParam("id") + " to an integer value.");
            return;
        }
        String username = authController.getUsername(ctx);
        System.out.println("User: " + username);
        System.out.println(employeeService.getEmployeeIDByUsername(username));
        if (employeeService.getEmployeeIDByUsername(username) == employeeID) {
            try {
                ctx.result(mapper.writeValueAsString(employeeService.getEmployeeByUsername(username)));
                return;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        if (authController.hasHRAccess(ctx)) {
            try {
                ctx.result(mapper.writeValueAsString(employeeService.getEmployeeByID(employeeID)));
                return;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        ctx.result("You are not authorized to view this data");
    }

    public void handlePutEmployeeRequest(Context ctx) {
        if (authController.hasHRAccess(ctx)) {
            Map<String, List<String>> formMap = ctx.formParamMap();
            if (employeeService.addNewEmployee(formMap)) {
                ctx.status(201);
                ctx.result("New employee successfully added.");
            }
            else {
                ctx.status(500);
                ctx.result("There was a problem adding the new employee.");
            }
        }
        else
            throw new UnauthorizedResponse("You do not have permission to hire new employees.");
    }

    public void handleDeleteEmployeeRequest(Context ctx) {
        if (authController.hasHRAccess(ctx)) {
            Map<String, List<String>> formMap = ctx.formParamMap();
            if (employeeService.deleteEmployeeRecord(formMap)) {
                ctx.status(200);
                ctx.result("Employee successfully deleted from database.");
            }
            else {
                ctx.status(500);
                ctx.result("There was a problem deleting the employee.");
            }
        }
        else
            throw new UnauthorizedResponse("You do not have permission to delete employees.");
    }
}
