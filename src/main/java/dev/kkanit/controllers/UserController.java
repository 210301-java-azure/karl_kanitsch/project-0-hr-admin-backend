package dev.kkanit.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kkanit.services.UserService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class UserController {

    private String username;
    private String password;
    private String email;
    private final UserService userService = new UserService();
    private final AuthController authController = new AuthController();
    private ObjectMapper mapper = new ObjectMapper();
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    public void handleNewUser(Context ctx) {
        if (ctx.formParamMap()==null || ctx.formParam("username")==null || ctx.formParam("password")==null || ctx.formParam("email")==null)
            throw new BadRequestResponse("Please fill in all required fields and try again.");
        username = ctx.formParam("username");
        password = ctx.formParam("password");
        email = ctx.formParam("email");
        int resultNumber = userService.addNewUser(username, password, email);
        switch (resultNumber) {
            case 0:
                throw new BadRequestResponse("User account not created. We are looking into why this happened.");
            case 1:
                ctx.result("User account with username: " + username + " created successfully");
                break;
            case 2:
                throw new BadRequestResponse("A user with that username already exists. Please try a unique username, or if this is your account, reset your password.");
            case 3:
                throw new BadRequestResponse("An account is already registered to that e-mail address. Please double check your e-mail address or reset your password.");
        }
    }

    public void handleChangePassword(Context ctx) {
        if (ctx.formParamMap()==null || ctx.formParam("username")==null || ctx.formParam("password")==null || ctx.formParam("new-password")==null)
            throw new BadRequestResponse("Please fill in all required fields and try again.");
        username = ctx.formParam("username");
        password = ctx.formParam("password");
        String newPassword = ctx.formParam("new-password");
        int resultNumber = userService.changeUserPassword(username, password, newPassword);
        switch (resultNumber) {
            case 0:
                throw new BadRequestResponse("There was a problem changing your password. We are investigating the cause.");
            case 1:
                ctx.result("Password successfully changed. Please log in again.");
                break;
            case 2:
                throw new BadRequestResponse("Invalid login credentials submitted");
        }
    }

    public void hashAllPasswords() {
        userService.hashAllPasswords();
    }

    public void handleGetUserRequest(Context ctx) {
        Map<String, List<String>> qMap;
        int qUserID = 0;
        int qPrivilege = 0;
        int qEmployeeID = 0;
        if (ctx.queryParamMap().size() == 0) {
            if (authController.hasAdminAccess(ctx)) {
                logger.info("UserController.handleGetUserRequest: Retrieving all user accounts for admin user: " + authController.getUsername(ctx));
                ctx.json(userService.getAllUserAccounts());
            }
            else if (authController.hasUserAccess(ctx)) {
                String username = authController.getUsername(ctx);
                logger.info("UserController.handleGetUserRequest: Retrieving user account for user: " + username);
                if (username != null)
                    ctx.json(userService.getUserAccount(username));
            }
            else
                throw new UnauthorizedResponse("You are not authorized to access this information");
        }
        else if (ctx.queryParamMap().containsKey("userid") || ctx.queryParamMap().containsKey("privilege") || ctx.queryParamMap().containsKey("employee-id")) {
            if (!authController.hasUserAccess(ctx))
                throw new UnauthorizedResponse("Only administrators can query all user accounts.");
            if (ctx.queryParamMap().containsKey("userid"))
                try {
                    qUserID = Integer.parseInt(ctx.queryParam("userid"));
                } catch (NumberFormatException e) {
                    logger.error("UserController.handleGetUserRequest: Could not parse userid: " + ctx.queryParam(
                            "userid") + " into an integer.");
                }

            if (ctx.queryParamMap().containsKey("privilege")) {
                try {
                    qPrivilege = Integer.parseInt(ctx.queryParam("privilege"));
                } catch (NumberFormatException e) {
                    logger.error("UserController.handleGetUserRequest: Could not parse privilege: " + ctx.queryParam(
                            "privilege") + " into an integer.");
                }
            }
            if (ctx.queryParamMap().containsKey("employee-id")) {
                try {
                    qEmployeeID = Integer.parseInt(ctx.queryParam("employee-id"));
                } catch (NumberFormatException e) {
                logger.error("UserController.handleGetUserRequest: Could not parse employee-id: " + ctx.queryParam(
                        "employee-id") +
                        " into an integer.");
                }
            }
            logger.info("UserController.handleGetUserRequest: Retrieving filtered user accounts for admin user: " + authController.getUsername(ctx));
            ctx.json(userService.getFilteredUserAccounts(qUserID, qPrivilege, qEmployeeID));
        }
    }

    public void handlePostUserRequest(Context ctx) {
        if (!authController.hasAdminAccess(ctx))
            throw new UnauthorizedResponse("Only administrators are allowed to link user accounts to employees");
        if (!ctx.formParamMap().containsKey("userid") || !ctx.formParamMap().containsKey("employee-id"))
            throw new BadRequestResponse("You must supply valid values for the userid and employee-id fields");
        int userID = 0;
        int employeeID = 0;
        try {
            userID = Integer.parseInt(ctx.formParam("userid"));
        } catch(NumberFormatException e) {
            logger.error("UserController.handlePostUserRequest: Could not parse userid: " + ctx.formParam(
                    "userid") + " into an integer.");
        }
        try {
            employeeID = Integer.parseInt(ctx.formParam("employee-id"));
        } catch(NumberFormatException e) {
            logger.error("UserController.handlePostUserRequest: Could not parse employee-id: " + ctx.formParam(
                    "employee-id") + " into an integer.");
        }
        if (userID < 1 || employeeID < 1)
            throw new BadRequestResponse("You must supply valid values for the userid and employee-id fields");
        logger.info("UserController.handlePostUserRequest: Attempting to link userid: " + userID + " to employee-id: " + employeeID);
        if(userService.linkUserAndEmployee(userID, employeeID))
            ctx.result("User and Employee linked successfully");
        else {
            ctx.status(500);
            ctx.result("There was a problem linking the provided userid to employee-id");
        }
    }
}
