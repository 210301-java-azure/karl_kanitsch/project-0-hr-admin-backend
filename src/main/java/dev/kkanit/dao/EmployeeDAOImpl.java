package dev.kkanit.dao;

import dev.kkanit.models.Employee;
import dev.kkanit.utilities.DAOUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO {

    private Connection connection = null;
    private PreparedStatement stmt = null;
    private Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);

    @Override
    public List<Employee> getAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "SELECT * FROM employee";
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Employee employee = new Employee();
                employee.setEmployeeID(rs.getInt("employee_id"));
                employee.setFirstName(rs.getString("first_name"));
                employee.setLastName(rs.getString("last_name"));
                employee.setHireDate(rs.getString("hire_date"));
                employee.setTerminationDate(rs.getString("termination_date"));
                employee.setActive(rs.getBoolean("active"));
                employee.setPayRate(rs.getDouble("pay_rate"));
                employee.setAccruedPTO(rs.getDouble("accrued_pto"));
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employees;
    }

    @Override
    public Employee getEmployee(int employeeID) {
        Employee employee = new Employee();
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "SELECT * FROM employee WHERE employee_id = ?";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1,employeeID);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                employee.setEmployeeID(rs.getInt("employee_id"));
                employee.setFirstName(rs.getString("first_name"));
                employee.setLastName(rs.getString("last_name"));
                employee.setHireDate(rs.getString("hire_date"));
                employee.setTerminationDate(rs.getString("termination_date"));
                employee.setActive(rs.getBoolean("active"));
                employee.setPayRate(rs.getDouble("pay_rate"));
                employee.setAccruedPTO(rs.getDouble("accrued_pto"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.info("Employee found: " + employee.toString());
        return employee;
    }

    @Override
    public boolean addEmployee(String firstName, String lastName, double payRate, String federalID) {
        boolean success = false;
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "INSERT INTO employee (first_name, " +
                    "last_name, hire_date, pay_rate, active, accrued_pto, federal_id) VALUES (" +
                    "?, ?, CURRENT_DATE, ?, TRUE, 0.0000, ?);";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1,firstName);
            stmt.setString(2, lastName);
            stmt.setDouble(3, payRate);
            stmt.setString(4,federalID);
            if (stmt.executeUpdate() != 0)
                success = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }

    @Override
    public boolean removeEmployee(int employeeID) {
        boolean success = false;
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "DELETE FROM employee WHERE employee_id = ?;";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1,employeeID);
            if (stmt.executeUpdate() != 0)
                success = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success;
    }
}
