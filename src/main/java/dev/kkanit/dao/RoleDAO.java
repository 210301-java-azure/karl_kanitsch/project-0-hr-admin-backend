package dev.kkanit.dao;

public interface RoleDAO {

    String getRoleName(int privilege);
    boolean hasUserAccess(int privilege);
    boolean hasHRAccess(int privilege);
    boolean hasAdminAccess(int privilege);
}
