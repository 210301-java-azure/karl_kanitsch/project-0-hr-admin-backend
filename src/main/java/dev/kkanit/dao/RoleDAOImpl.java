package dev.kkanit.dao;

import dev.kkanit.utilities.DAOUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleDAOImpl implements RoleDAO {

    private Connection connection = null;
    private PreparedStatement stmt = null;
    private Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);

    @Override
    public String getRoleName(int privilege) {
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "SELECT name FROM user_role WHERE id = ?";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1,privilege);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getString("name");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "No Role Found";
    }

    @Override
    public boolean hasUserAccess(int privilege) {
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "SELECT has_user_access FROM user_role WHERE id = ?";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1,privilege);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("has_user_access");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean hasHRAccess(int privilege) {
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "SELECT has_hr_access FROM user_role WHERE id = ?";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1,privilege);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("has_hr_access");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean hasAdminAccess(int privilege) {
        try (Connection connection = DAOUtilities.getConnection()) {
            String sql = "SELECT has_admin_access FROM user_role WHERE id = ?";
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1,privilege);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("has_admin_access");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
