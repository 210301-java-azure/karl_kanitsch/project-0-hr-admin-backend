package dev.kkanit.dao;

import dev.kkanit.models.UserAccount;
import dev.kkanit.utilities.DAOUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDAOImpl implements UserDAO {

    private Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

    public List<String> getAllUsernames() {
        List<String> usernames = new ArrayList<>();
        String sql = "SELECT user_name FROM user_account";
        try (Connection connection = DAOUtilities.getConnection()){
            if (connection == null)
                logger.warn("UserDAOImpl.getAllUsernames() failed to acquire a connection to the database.");
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                usernames.add(rs.getString("user_name"));
            }
        } catch (SQLException e) {
            logger.error("ERROR in UserDAOImpl.getAllUserNames(): " + e.getClass() + "  " + e.getMessage());
        }
        return usernames;
    }

    public List<String> getAllEmails() {
        List<String> emails = new ArrayList<>();
        String sql = "SELECT email FROM user_account";
        try (Connection connection = DAOUtilities.getConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                emails.add(rs.getString("email"));
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return emails;
    }

    public String getUserPassword(String username) {
        String sql = "SELECT password FROM user_account WHERE user_name = ?";
        try (Connection connection = DAOUtilities.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                String dbPassword = rs.getString("password");
                logger.info("Password retrieved from the database.");
                return dbPassword;
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return null;
    }

    public Map<Integer,UserAccount> getAllUserAccounts() {
        Map<Integer,UserAccount> userMap = new HashMap<>();
        String sql = "SELECT * FROM user_account";
        try (Connection connection = DAOUtilities.getConnection()){
            if (connection == null)
                logger.warn("UserDAOImpl.getAllUserAccounts() failed to acquire a connection to the database.");
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                UserAccount user = new UserAccount();
                user.setUsername(rs.getString("user_name"));
                user.setEmail(rs.getString("email"));
                user.setPrivilege(rs.getInt("privilege"));
                user.setEmployeeID(rs.getInt("employee_id"));
                userMap.put(rs.getInt("userid"),user);
            }
        } catch (SQLException e) {
            logger.error("ERROR in UserDAOImpl.getAllUserNames(): " + e.getClass() + "  " + e.getMessage());
        }
        return userMap;
    }

    public Map<Integer,UserAccount> getFilteredUserAccounts(String sql, List<Integer> ids) {
        Map<Integer, UserAccount> userMap = new HashMap<>();

        try (Connection connection = DAOUtilities.getConnection()){
            if (connection == null)
                logger.warn("UserDAOImpl.getFilteredUserAccounts() failed to acquire a connection to the database.");
            PreparedStatement stmt = connection.prepareStatement(sql);
            int index = 1;
            for (int id: ids) {
                stmt.setInt(index, id);
                index++;
            }
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                UserAccount user = new UserAccount();
                user.setUsername(rs.getString("user_name"));
                user.setEmail(rs.getString("email"));
                user.setPrivilege(rs.getInt("privilege"));
                user.setEmployeeID(rs.getInt("employee_id"));
                userMap.put(rs.getInt("userid"),user);
            }
        } catch (SQLException e) {
            logger.error("ERROR in UserDAOImpl.getFilteredUserNames(): " + e.getClass() + "  " + e.getMessage());
        }
        return userMap;
    }

    public UserAccount getUserAccount(String username) {
        UserAccount user = new UserAccount();
        String sql = "SELECT * FROM user_account WHERE user_name = ?";
        try (Connection connection = DAOUtilities.getConnection()){
            if (connection == null)
                logger.warn("UserDAOImpl.getAllUserAccounts() failed to acquire a connection to the database.");
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                user.setUsername(rs.getString("user_name"));
                user.setEmail(rs.getString("email"));
                user.setPrivilege(rs.getInt("privilege"));
                user.setEmployeeID(rs.getInt("employee_id"));
            }
        } catch (SQLException e) {
            logger.error("ERROR in UserDAOImpl.getAllUserNames(): " + e.getClass() + "  " + e.getMessage());
        }
        return user;
    }

    public boolean updateUserEmployeeID(int userID, int employeeID) {
        String sql = "UPDATE user_account SET employee_id = ? WHERE userid = ?";
        try (Connection connection = DAOUtilities.getConnection()){
            if (connection == null)
                logger.warn("UserDAOImpl.updateUserEmployeeID failed to acquire a connection to the database.");
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, employeeID);
            stmt.setInt(2, userID);
            if (stmt.executeUpdate() != 0) {
                logger.info("UserDAOImpl.updateUserEmployeeID: Linking successful.");
                return true;
            }
        } catch (SQLException e) {
            logger.error("ERROR in UserDAOImpl.updateUserEmployeeID: " + e.getClass() + "  " + e.getMessage());
        }
        return false;
    }

    public int getPrivilege(String username) {
        String sql = "SELECT privilege FROM user_account WHERE user_name = ?";
        try (Connection connection = DAOUtilities.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int privilege = rs.getInt("privilege");
                logger.info("User " + username + " has privilege Rank: " + privilege);
                return privilege;
            }
        } catch (SQLException e) {
            logger.error("ERROR in getPrivilege method");
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return 0;
    }

    public int getEmployeeID(String username) {
        String sql = "SELECT employee_id FROM user_account WHERE user_name = ?";
        try (Connection connection = DAOUtilities.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int employeeID = rs.getInt("employee_id");
                logger.info("User " + username + " is linked to employee_id: " + employeeID);
                return employeeID;
            }
        } catch (SQLException e) {
            logger.error("ERROR in getEmployeeID method");
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return 0;
    }

    public boolean addNewUser(String username, String password, String email) {
        boolean success = false;
        String sql = "INSERT INTO user_account (user_name, password, email, privilege) VALUES (?,?,?,1)";
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String newPassword = passwordEncoder.encode(password);
        try (Connection connection = DAOUtilities.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,username);
            stmt.setString(2,newPassword);
            stmt.setString(3,email);
            if (stmt.executeUpdate() != 0)
                success = true;
        } catch (SQLException e) {
            logger.error("ERROR in addNewUser method");
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return success;
    }

    public boolean changeUserPassword(String username, String password) {
        boolean success = false;
        String sql = "UPDATE user_account SET password = ? WHERE user_name = ?";
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String newPassword = passwordEncoder.encode(password);
        try (Connection connection = DAOUtilities.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1,newPassword);
            stmt.setString(2,username);
            if (stmt.executeUpdate() != 0)
                success = true;
        } catch (SQLException e) {
            logger.error("ERROR in changeUserPassword method");
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return success;
    }
}
