package dev.kkanit.models;

public class UserAccount {

    private String username;
    private int privilege;
    private String email;
    private int employeeID;
    public UserAccount() {}
    public UserAccount(String username, int privilege, String email, int employeeID) {
        this.username = username;
        this.privilege = privilege;
        this.email = email;
        this.employeeID = employeeID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPrivilege() {
        return privilege;
    }

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }
}
