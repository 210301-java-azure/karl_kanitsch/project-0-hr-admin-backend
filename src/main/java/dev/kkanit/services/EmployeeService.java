package dev.kkanit.services;

import dev.kkanit.dao.EmployeeDAO;
import dev.kkanit.dao.EmployeeDAOImpl;
import dev.kkanit.dao.UserDAO;
import dev.kkanit.dao.UserDAOImpl;
import dev.kkanit.models.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class EmployeeService {

    private EmployeeDAO eDAO = new EmployeeDAOImpl();
    private UserDAO uDAO = new UserDAOImpl();
    private Logger logger = LoggerFactory.getLogger(EmployeeService.class);

    public List<Employee> getAllEmployees() {
        List<Employee> employees = eDAO.getAllEmployees();
        return employees;
    }

    public Employee getEmployeeByUsername(String username) {
        int employeeID = uDAO.getEmployeeID(username);
        return eDAO.getEmployee(employeeID);
    }

    public int getEmployeeIDByUsername(String username) {
        int employeeID = uDAO.getEmployeeID(username);
        return employeeID;
    }

    public Employee getEmployeeByID(int employeeID) {
        return eDAO.getEmployee(employeeID);
    }

    public boolean addNewEmployee(Map<String, List<String>> formMap) {
        boolean success = false;
        if (formMap.containsKey("first-name") &&
                formMap.containsKey("last-name") &&
                formMap.containsKey("pay-rate") &&
                formMap.containsKey("federal-id")) {
            String firstName = formMap.get("first-name").get(0);
            String lastName = formMap.get("last-name").get(0);
            Double payRate;
            try {
                payRate = Double.parseDouble(formMap.get("pay-rate").get(0));
            } catch(NumberFormatException e) {
                logger.error("Could not parse " + formMap.get("pay-rate").get(0) + " into a double.");
                return false;
            }
            String federalID = formMap.get("federal-id").get(0);
            if (eDAO.addEmployee(firstName, lastName, payRate, federalID))
                success = true;
        }
        return success;
    }

    public boolean deleteEmployeeRecord(Map<String, List<String>> formMap) {
        boolean success = false;
        if (formMap.containsKey("employee-id")) {
            int employeeID = 0;
            try {
                employeeID = Integer.parseInt(formMap.get("employee-id").get(0));
            } catch (NumberFormatException e) {
                logger.error("Could not parse " + formMap.get("employee-id").get(0) + " into an integer");
            }
            if (eDAO.removeEmployee(employeeID))
                success = true;
        }
        logger.info(Boolean.toString(success));
        return success;
    }
}
