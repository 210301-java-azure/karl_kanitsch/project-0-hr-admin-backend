package dev.kkanit.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import dev.kkanit.dao.UserDAO;
import dev.kkanit.dao.UserDAOImpl;
import dev.kkanit.models.UserAccount;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import javalinjwt.JWTGenerator;
import javalinjwt.JWTProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

//import java.net.PasswordAuthentication;
//import java.nio.charset.StandardCharsets;
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.security.SecureRandom;
import java.security.Key;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserService {

    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final UserDAO uDAO = new UserDAOImpl();
    private static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    public int authenticate(String username, String password) {
        logger.info("Attempting to authenticate user.");
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        int privilege = 0;

        List<String> usernames = uDAO.getAllUsernames();
        if (!usernames.contains(username)) {
            logger.info("Unmatched username: " + username + " supplied during login");
            return 0;
        }
        String dbPassword = uDAO.getUserPassword(username);
        if (dbPassword==null) {
            logger.warn("No password retrieved.");
            return 0;
        }
        if (passwordEncoder.matches(password,dbPassword))
            privilege = uDAO.getPrivilege(username);
        return privilege;
    }

    public String generateAuthToken(String username, int privilege) {
        Map<String, String> claimMap = new HashMap<>();
        claimMap.put("username", username);
        claimMap.put("privilege", Integer.toString(privilege));
        String jws = Jwts.builder().setClaims(claimMap).signWith(key).compact();
        logger.info("Created JWS: " + jws);
        return jws;
    }

    public Map<String, Object> validateAuthToken(String authToken) {
        try {
            return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(authToken).getBody();
        } catch (Exception e) {
            logger.error("SignatureException encountered while validating login token.");
        }
        return new HashMap<String, Object>();
    }

    public int addNewUser(String username, String password, String email) {
        List<String> usernames = uDAO.getAllUsernames();
        if (usernames.contains(username)) {
            logger.info("Matched username: " + username + " .\n\tUser already exists");
            return 2;
        }
        List<String> emails = uDAO.getAllEmails();
        if (emails.contains(email)) {
            logger.info("Matched email: " + email + " .\n\tUser already exists");
            return 3;
        }
        if (uDAO.addNewUser(username, password, email))
            return 1;
        return 0;
    }

    public int changeUserPassword(String username, String password, String newPassword) {
        if(authenticate(username, password) <= 0)
            return 2;
        if(uDAO.changeUserPassword(username, newPassword))
            return 1;
        return 0;
    }

    public int getEmployeeID(String username) {
        return uDAO.getEmployeeID(username);
    }

    public Map<Integer,UserAccount> getAllUserAccounts () {
        logger.info("Retrieving all user accounts");
        Map<Integer, UserAccount> userMap = uDAO.getAllUserAccounts();
        return userMap;
    }

    public Map<Integer,UserAccount> getFilteredUserAccounts(int qUserID, int qPrivilege, int qEmployeeID) {
        Map<Integer, UserAccount> userMap = new HashMap<>();
        if (qUserID==0 && qPrivilege==0 && qEmployeeID==0)
            return userMap;
        int qCount = 0;
        List<Integer> ids = new ArrayList<>();
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT * FROM user_account WHERE ");
        if (qUserID != 0) {
            qCount++;
            sqlBuilder.append("userid = ?");
            ids.add(qUserID);
        }
        if (qPrivilege != 0) {
            if (qCount > 0)
                sqlBuilder.append(" and ");
            qCount++;
            sqlBuilder.append("privilege = ?");
            ids.add(qPrivilege);
        }
        if (qEmployeeID != 0) {
            if (qCount > 0)
                sqlBuilder.append(" and ");
            qCount++;
            sqlBuilder.append("employee_id = ?");
            ids.add(qEmployeeID);
        }
        String sql = sqlBuilder.toString();
        logger.info("UserService.getFilteredUserAccounts executing SQL Statement: " + sql);
        return uDAO.getFilteredUserAccounts(sql, ids);
    }

    public UserAccount getUserAccount(String username) {
        logger.info("Retrieving user account for " + username);
        return uDAO.getUserAccount(username);
    }

    public boolean linkUserAndEmployee(int userID, int employeeID) {
        return uDAO.updateUserEmployeeID(userID, employeeID);
    }

    public void hashAllPasswords() {
        List<String> usernames = uDAO.getAllUsernames();
        for (String user: usernames) {
            String password = uDAO.getUserPassword(user);
            uDAO.changeUserPassword(user, password);
        }
    }

    // My original hashing implementation using SHA-512
//    public static int authenticate(String username, String password) {
//        logger.info("Attempting to authenticate user.");
//        SecureRandom r = new SecureRandom();
//        byte[] salt = new byte[16];
//        int privilege = 0;
//
//        List<String> usernames = uDAO.getAllUsernames();
//        if (!usernames.contains(username)) {
//            logger.info("Unmatched username: " + username + " supplied during login");
//            return 0;
//        }
//        byte[] dbPassword = uDAO.getUserPassword(username);
//        if (dbPassword==null) {
//            logger.warn("No password retrieved.");
//            return 0;
//        }
//        r.nextBytes(salt);
//        try {
//            MessageDigest md = MessageDigest.getInstance("SHA-512");
//            md.update(salt);
//            byte[] hPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
//        } catch (NoSuchAlgorithmException e) {
//            logger.error("Hashing algorithm was not found. Authentication attempt aborted");
//            e.printStackTrace();
//            return 0;
//        }
//        privilege = uDAO.getPrivilege(username);
//        return privilege;
//    }



}
